<form action="" class="tire-filter-form">
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-2 col-lg-2">
            <div class="form-group">
                <label for="tire-filter-manufacturer_id">Производитель</label>
                <select name="manufacturer_id" id="tire-filter-manufacturer_id" class="form-control">
                    <option value=""> -- Выберите --</option>
                    <?php foreach ($data_helper->getManufacturers() as $key => $value): ?>
                        <option <?=($filter['manufacturer_id'] == $key ? 'selected' : '')?> value="<?=$key?>"><?=$value?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-2 col-lg-2">
            <div class="form-group">
                <label for="tire-filter-diameter_id">Радиус</label>
                <select name="diameter_id" id="tire-filter-diameter_id" class="form-control">
                    <option value=""> -- Выберите --</option>
                    <?php foreach ($data_helper->getDiameters() as $key => $value): ?>
                        <option <?=($filter['diameter_id'] == $key ? 'selected' : '')?> value="<?=$key?>"><?=$value?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-2 col-lg-2">
            <div class="form-group">
                <label for="tire-filter-width_id">Ширина</label>
                <select name="width_id" id="tire-filter-width_id" class="form-control">
                    <option value=""> -- Выберите --</option>
                    <?php foreach ($data_helper->getWidths() as $key => $value): ?>
                        <option <?=($filter['width_id'] == $key ? 'selected' : '')?> value="<?=$key?>"><?=$value?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-2 col-lg-2">
            <div class="form-group">
                <label for="tire-filter-profile_id">Профиль</label>
                <select name="profile_id" id="tire-filter-profile_id" class="form-control">
                    <option value=""> -- Выберите --</option>
                    <?php foreach ($data_helper->getProfiles() as $key => $value): ?>
                        <option <?=($filter['profile_id'] == $key ? 'selected' : '')?> value="<?=$key?>"><?=$value?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-2 col-lg-2">
            <div class="form-group">
                <label for="tire-filter-season">Сезон</label>
                <select name="season" id="tire-filter-season" class="form-control">
                    <option value=""> -- Выберите --</option>
                    <?php foreach ($data_helper->getSeasons() as $key => $value): ?>
                        <option <?=($filter['season'] == $key ? 'selected' : '')?> value="<?=$key?>"><?=$value?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-2 col-lg-2">
            <div class="form-group">
                <button type="submit" class="btn btn-info" id="tire-filter-submit">Фильтр</button>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-2 col-lg-2">

        </div>
    </div>
</form>