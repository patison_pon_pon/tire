<?php
$query = "SELECT 
       tire.*, 
       td.name AS diameter,
       tm.name AS manufacturer,
       tp.name AS profile,
       tw.name AS width,
       (SELECT src FROM tire_photo WHERE pp_id = tire.id ORDER BY sort LIMIT 1) as src
    FROM tire 
        LEFT JOIN tire_diameter AS td ON tire.diameter_id = td.id
        LEFT JOIN tire_manufacturer AS tm ON tire.manufacturer_id = tm.id
        LEFT JOIN tire_profile AS tp ON tire.profile_id = tp.id
        LEFT JOIN tire_width AS tw ON tire.width_id = tw.id
    ";
$count_query = "SELECT COUNT(*) FROM tire ";
$where = '';

if ($filter['manufacturer_id'])
    $where .= " manufacturer_id=" . $filter['manufacturer_id'] . ' and ';

if ($filter['diameter_id'])
    $where .= " diameter_id=" . $filter['diameter_id'] . ' and ';

if ($filter['width_id'])
    $where .= " width_id=" . $filter['width_id'] . ' and ';

if ($filter['profile_id'])
    $where .= " profile_id=" . $filter['profile_id'] . ' and ';

if ($filter['season'])
    $where .= " season=" . $filter['season'] . ' and ';

if ($where){
    $query .= " WHERE" . substr($where, 0,-5);
    $count_query .= " WHERE" . substr($where, 0,-5);
}
$count = mysqli_fetch_assoc(mysqli_query($db, $count_query));
$count = (int) $count['COUNT(*)'];
$query .= " ORDER BY name";

if ($filter['limit'] && $filter['page'])
    $query .= " LIMIT " . $filter['limit'] . ' OFFSET ' . $filter['offset'];
else
    $query .= " LIMIT 10";


$r = mysqli_query($db, $query);
$seasons = $data_helper->getSeasons();
while ($f = mysqli_fetch_assoc($r)): ?>
    <div class="product">
        <h2><?=$f['name']?></h2>
        <p>Производитель: <?=$f['manufacturer']?></p>
        <p>радиус: <?=$f['diameter']?></p>
        <p>профиль: <?=$f['profile']?></p>
        <p>ширина: <?=$f['width']?></p>
        <p>сезон: <?=$seasons[$f['season']]?></p>
        <img src="<?=ROOT?>img/other/<?=$f['src']?>" width="100">
        <a href="<?=ROOT?>index.php?type=tire&alias=<?=$f['alias']?>" class="btn btn-success">Подробнее</a>
        <?php var_dump($f); ?>
    </div>
<?php endwhile; ?>
<?php
function paginator($filter, $count){
    $pages = ceil(($count/$filter['limit']));
    if ($pages == 1)
        return null;

    $html = '';
    $html .= '<section id="paginator">';
    if ($filter['page'] == 1){
        $html .= '<button class="prev" disabled><</button>';
        $html .= '<button disabled class="active">1</button>';
    }else{
        $html .= '<button class="prev" data-page="' . ($filter['page'] - 1) . '"><</button>';
        $html .= '<button data-page="1">1</button>';
    }
    $dottedBefore = 0;

    for ($i = 2; $i < $pages; $i++){

        if ($i === $pages) {
            continue;
        }

        if(($filter['page'] - 2) > 1 && !$dottedBefore){
            $html .= '<span>...</span>';
            $dottedBefore = true;
        }

        if (($filter['page'] - $i) == 1) {
            $html .= '<button data-page="' . $i . '">' . $i . '</button>';
        }
        if (($i - $filter['page']) == 1) {
            $html .= '<button data-page="' . $i . '">' . $i . '</button>';
        }
        if ($filter['page'] == $i && $filter['page'] != 1 && $filter['page'] != $pages) {
            $html .= '<button disabled class="active">' . $i . '</button>';
        }
    }

    if (($pages - $filter['page']) > 2) {
        $html .= '<span>...</span>';
    }

    if ($filter['page'] == $pages){
        $html .= '<button class="active">' . $pages .'</button>';
        $html .= '<button class="next" disabled>></button>';
    }else{
        if (!$pages){
            $html .= '<button class="next" disabled>></button>';
        }else{
            $html .= '<button data-page="' . $pages . '">' . $pages . '</button>';
            $html .= '<button class="next"  data-page="' . ($filter['page'] +1) . '">></button>';
        }
    }
    $html .= '</section>';
    return $html;
}

echo paginator($filter, $count);
