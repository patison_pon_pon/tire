<?php
include '../config.php';
include '../library/main.php';
include '../helpers/DataHelper.php';

$data_helper = new DataHelper($db);
$request = $_POST;
$action = (isset($_POST['action']) ? $_POST['action'] : null);
$filter = [
    'manufacturer_id' => ($request['manufacturer_id'] ? $request['manufacturer_id'] : null),
    'diameter_id' => ($request['diameter_id'] ? $request['diameter_id'] : null),
    'width_id' => ($request['width_id'] ? $request['width_id'] : null),
    'profile_id' => ($request['profile_id'] ? $request['profile_id'] : null),
    'season' => ($request['season'] ? $request['season'] : null),
    'page' => ($request['page'] ? $request['page'] : null),
    'limit' => ($request['limit'] ? $request['limit'] : null),
];
$filter['offset'] = $filter['limit'] * ($filter['page'] - 1);

switch ($action){
    case 'draw_filter':
        $data = include "template/filter.php";
        break;

    case 'go_filter':
        $data = include "template/catalog.php";
        break;
}
