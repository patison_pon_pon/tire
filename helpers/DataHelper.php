<?php


class DataHelper
{
    protected $db;

    function __construct($db){
        //подключение к базе
        $this->db = $db;
    }

    public function getManufacturers(){
        $data = [];
        $r = mysqli_query($this->db, "SELECT * FROM tire_manufacturer WHERE language_id = '1' ORDER BY name");
        while ($f = mysqli_fetch_assoc($r)){
            $data[$f['id']] = $f['name'];
        }
        return $data;
    }

    public function getDiameters(){
        $data = [];
        $r = mysqli_query($this->db, "SELECT * FROM tire_diameter WHERE language_id = '1' ORDER BY name");
        while ($f = mysqli_fetch_assoc($r)){
            $data[$f['id']] = $f['name'];
        }
        return $data;
    }

    public function getProfiles(){
        $data = [];
        $r = mysqli_query($this->db, "SELECT * FROM tire_profile WHERE language_id = '1' ORDER BY name");
        while ($f = mysqli_fetch_assoc($r)){
            $data[$f['id']] = $f['name'];
        }
        return $data;
    }

    public function getWidths(){
        $data = [];
        $r = mysqli_query($this->db, "SELECT * FROM tire_width WHERE language_id = '1' ORDER BY name");
        while ($f = mysqli_fetch_assoc($r)){
            $data[$f['id']] = $f['name'];
        }
        return $data;
    }

    public function getSeasons(){
        return [
            1 => 'Зима',
            2 => 'Лето',
            3 => 'Всесезон',
        ];
    }
}