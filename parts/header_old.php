<!DOCTYPE html>
<html>

<head>
    <title>
        <?=$myBase['meta_t']?>
    </title>
    <?php 
	if($myBase['meta_d'] != '' || $myBase['meta_d'] != NULL)
		echo "<meta name='description' content='{$myBase['meta_d']}'>"; 
	if($myBase['meta_k'] != '' || $myBase['meta_k'] != NULL)
		echo "<meta name='keywords' content='{$myBase['meta_k']}'>"; 
	?>
        <meta charset="UTF-8">
<!--        <meta name="viewport" content="width=device-width, initial-scale=1.0">-->
           <meta name="viewport" content="width=1276, maximum-scale=1">
        <link rel="stylesheet" href="<?=ROOT?>assets/css/bootstrap.css">
        <link rel="stylesheet" href="<?=ROOT?>assets/css/common.css">
        <link rel="stylesheet" href="<?=ROOT?>assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?=ROOT?>assets/css/docs.css">
        <link rel="stylesheet" href="<?=ROOT?>assets/css/bootstrap-social.css">
<!--        <link rel="stylesheet" href="<?=ROOT?>assets/css/style.css">-->
        <link rel="stylesheet" href="<?=ROOT?>assets/css/owl-carousel.css">
        <link rel="stylesheet" href="<?=ROOT?>assets/css/jgallery.min.css?v=1.5.0">
        <link rel="stylesheet" href="<?=ROOT?>assets/css/lightbox.css">
        <link rel="stylesheet" href="<?=ROOT?>assets/css/animate.css">
<!--        <script src="<?=ROOT?>assets/js/jquery.min.js"></script>-->
            <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">


 <link href="<?=ROOT?>assets/css/normalize.css" rel="stylesheet">
    <link href="<?=ROOT?>assets/css/libs.min.css" rel="stylesheet">
    <link href="<?=ROOT?>assets/css/main.css" rel="stylesheet">

          <link rel="stylesheet" href="<?=ROOT?>assets/css/style.css">
    <link rel="stylesheet" href="<?=ROOT?>assets/css/new.css">
    <link href="<?=ROOT?>assets/js/datetimepicker/jquery.datetimepicker.css" rel="stylesheet"> 
    
      <link rel="shortcut icon" href="<?=ROOT?>favicon.png" />

</head>

<body>
<!--  <?php include 'parts/other/language.php'; ?>-->
   <header>
            <div class="top-header">
                <div class="content">
                    <div class="select-city-wrapper"></div> 
                    <div class="contacts-block">
                        <div class="block geo"><a href="mailto:info@top-sochi.ru"><i style="display: inline-block;"></i>info@top-sochi.ru</a></div> <a href="tel:+79388881977" class="block phone"><i></i>+7 (938) 888-19-77</a> </div>
                </div>
            </div>
            <div class="general-header">
                <div class="content">
                    <a href="/" class="logo"></a>
                    <div class="menu"> 
                   <a href="<?=ROOT?>pages/arenda-avto-s-voditelem">Авто с водителем</a> 
                  <a href="<?=ROOT?>pages/arenda-yakht" style="color: #227dd9;">Аренда Яхт</a> 
                  <a href="<?=ROOT?>pages/arenda-vertoletov" style="color: #a330ba;">Аренда вертолетов</a> 
                       
                  <a href="<?=ROOT?>pages/o-kompanii">О компании</a> 
<!--                  <a href="<?=ROOT?>pages/ehtapy-sdelki">Авто без водителя</a> -->
            
<!--
                    <div class="select">
					<span>Прокат авто</span>
					<div class="options">
						<a href="<?=ROOT?>pages/arenda-avto-s-voditelem">Прокат с водителем</a>
						<a href="<?=ROOT?>pages/ehtapy-sdelki">Прокат без водителя</a>
					</div>
				</div>
                    
-->
                    <a href="<?=ROOT?>pages/galereya" style="color: #f7901e;">ГАЛЕРЕЯ</a>  <a href="<?=ROOT?>pages/kontakty">Контакты</a> 
                    <a href="/pages/uslugi-i-ceny" style="      color: rgba(255, 255, 255, 0.94);
    background: #f7911e;
    padding: 6px 20px;
    border-radius: 0px;
    padding-bottom: 6px;
    font-size: 16px;
    /* width: 100%; */
    text-align: left;
    margin: 0;
    padding: 0;
    padding-left: 15px;
    padding-right: 15px;
    padding-top: 6px;
    transform: translateY(-3px);">Услуги/Цены </a>    
    <a href="/pages/platjozhnaya-stranica" style="      color: #27b257;


 
    font-size: 16px;
    /* width: 100%; */
    text-align: left;
    margin: 0;
    padding: 0;
    padding-left: 15px;
    padding-right: 15px;
    padding-top: 6px;
    transform: translateY(-3px);">Онлайн оплата</a>
                   </div>
                    <div class="hamburger hamburger--3dx">
                        <div class="hamburger-box">
                            <div class="hamburger-inner"></div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="mobile-nav">
            <div class="hamburger hamburger--3dx"> 
                <div class="hamburger-box">
                    <div class="hamburger-inner"></div>
                </div>
            </div>
            <div class="wrapper">
                  <div class="menu"> 
                  <a href="site/cars.html">Выбор авто</a> 
                  <a href="#">Аренда Яхт</a> 
                  <a href="#">Аренда вертолетов</a> 
                     <a href="#">Прокат авто</a> 
              
                  <a href="#">Прокат авто</a> 
<!--                    <a href="/conditions.html">Условия аренды</a> -->
<!--
                    <div class="select">
					<span>Условия аренды</span>
					<div class="options">
						<a href="/arenda_s_voditelem.html">Аренда с водителем</a>
						<a href="/conditions.html">Прокат без водителя</a>
					</div>
				</div>
--> 
                    
                    <a href="/pages/otzyvy">Отзывы</a> 
                   <a href="#">Контакты</a> 
                     <a href="#">Услуги/Цены</a>
                     <a href="#"></a>
                     </div>
                <div class="contacts-block">
                    <div class="block geo"><i></i>Сочи</div> <a href="" class="block phone"><i></i>+7 (938) 888-19-77</a> </div>
            </div>
        </div>
<!--
    <header id="header">


        <div class="container-fluid header-c" style="    padding-bottom: 20px; padding-top:20px;">
            <div class="row" class="header-content">
                <div class="col-xs-12 col-sm-5 col-sm-offset-1 ">
                    <a href="<?=ROOT?>"> <img src="/img/orlogo.png" height="100px" alt=""> </a>
                    				<?php include 'parts/other/search.php'; ?>
                </div>
                <div class="col-xs-12 text-right col-sm-5 ">
               
                </div>
            </div>
        </div>
        <?php drawBlock(6); ?>
    </header>
-->
<!--    <?php drawMenu(1); ?>-->
       <div style="
    clear:  both;
    margin-top: 121px;
"> </div>
       <style>
    .breadcrumb {
    padding: 8px 15px;
    margin-bottom: 0px;
    list-style: none;
    background-color: #f5f5f5;
    border-radius: 4px;
}
           .breadcrumb li {
    display: inline-block;
    font-size: 12px;
}
    </style> 
        <?php include 'parts/breadcrumbs.php'; ?>