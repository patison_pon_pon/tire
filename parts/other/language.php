<style>
    .lang-form {
        top: 0;
        position: fixed;
        right: 0;
    }
    
    .lang {
        height: 33px;
        width: auto;
        background: #ffb500;
        color: #fff;
        font-weight: 900;
        font-size: 20px;
    }
    
    .change-language {
           top: 24px;
    position: fixed;
    z-index: 9999;
    right: 0.5%;
        
    }

    
</style>


   <style>

    .btn__p {
         border-radius: 25px;
    position: relative;
    display: block;
    margin-top: 0px;
    margin-bottom: 0px;
    margin-left: 0px;
    margin-right: 10px;
    display: inline-block;
    /* border: 2px solid #2b2b2b85; */
    border-right: 0;
    /* border-radius: 44px; */
    /* box-shadow: 0px 0px 10px #5b7dbd; */
    transition: 0.3s;
    padding: 0;
    opacity: 0.8;
    transition: 0.3s;
    border: none;

    }  
       .btn__p:hover {
  transition: 0.3s;
            opacity:1;

    } 
/*
    .lang__box > a:hover {
transition: 0.3s;
        transform: scale(1.1);
    } 
*/
      .btn__p > img  {
    height: 27px;
    width: 52px;
    }
       
       
           
    @media (max-width:768px) {
        .change-language {
    top: 14px;
    position: fixed;
    z-index: 9999;
    right: 50%;
    margin-right: -95px;
}
           .btn__p > img  {
    height: 22px;
    width: 48px;
    }
        .btn__p {
/*
    position: relative;
    display: block;
    margin-top: 0px;
    margin-bottom: 0px;
    margin-left: 10px;
    margin-right: 18px;
    display: inline-block;
    border: 2px solid #2b2b2b85;
    border-right: 0;
    border-radius: 3px;
    box-shadow: 0px 0px 10px #5b7dbd;
    transition: 0.3s;
    padding: 0;
    opacity: 0.8;
    transition: 0.3s;
*/
                position: relative;
    display: block;
    margin-top: 0px;
    margin-bottom: 0px;
    margin-left: 10px;
    margin-right: 18px;
    display: inline-block;
    border: 2px solid #2b2b2b85;
    border-right: 0;
    border-radius: 3px;
    box-shadow: 0px 0px 1px #9ea0a2;
    transition: 0.3s;
    padding: 0;
    opacity: 1;
    transition: 0.3s;
    border: none;
}
    }
</style>



<form id="lang" method="post" name="lang" class="lang-form" style="   
    z-index: 0;   opacity: 0;
">
    <select name="lang" class="lang" onchange="$('#lang').submit()">
        <option value="1" <?=($_SESSION['lang']=="1" ? "selected": '')?> class="option">RU</option>
        <option value="2" <?=($_SESSION['lang']=="2" ? "selected": '')?> class="option">EN</option>
    </select>
</form>

<?php
if (isset($_POST['lang'])) {
    setcookie('lang', $_POST['lang'], time()+31536000, '/');
    $_SESSION['lang'] = $_POST['lang'];
    exit("<meta http-equiv='refresh' content='0; url= $_SERVER[REQUEST_URI]'>");
}
?>
  
  
<div class="change-language">
    <button type="button" class="btn__p" data-language_id='1'>
        <img src="http://top-sochi.ru/img/rus__img.png" alt="">
    </button>   
       
    <button type="button" class="btn__p" data-language_id='2'>
        <img src="http://top-sochi.ru/img/en__img.png" alt="">
    </button>
</div>

<script>
    $(document).ready( function() {
        var current_language_id = $('.lang').val();
            console.log(current_language_id);
        $('.change-language').find('[data-language_id="' + current_language_id + '"]').addClass('active');
        $('.change-language button').on('click', function () {
            var language_id = $(this).data('language_id');
            console.log(language_id);
            $('.lang').val(language_id);
            $('#lang').submit();
        })
    })
</script>