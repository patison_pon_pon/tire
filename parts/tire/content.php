<?php
$alias = $_GET['alias'];
include 'helpers/DataHelper.php';
$data_helper = new DataHelper($db);
$seasons = $data_helper->getSeasons();

$query = "SELECT 
       tire.*, 
       td.name AS diameter,
       tm.name AS manufacturer,
       tp.name AS profile,
       tw.name AS width,
       (SELECT src FROM tire_photo WHERE pp_id = tire.id ORDER BY sort LIMIT 1) as src
    FROM tire 
        LEFT JOIN tire_diameter AS td ON tire.diameter_id = td.id
        LEFT JOIN tire_manufacturer AS tm ON tire.manufacturer_id = tm.id
        LEFT JOIN tire_profile AS tp ON tire.profile_id = tp.id
        LEFT JOIN tire_width AS tw ON tire.width_id = tw.id
    where tire.alias = '$alias'
    ";
echo $query;
$tire = mysqli_fetch_assoc(mysqli_query($db, $query));
$r = mysqli_query($db, "SELECT * FROM tire_photo WHERE pp_id = " . $tire['id'] . " ORDER BY sort");
$gallery = [];
while($f = mysqli_fetch_assoc($r)){
    $gallery[] = $f;
}

?>
<div class="container-fluid">
	<div class="row">

        <h2><?=$f['name']?></h2>
        <p>Производитель: <?=$tire['manufacturer']?></p>
        <p>радиус: <?=$tire['diameter']?></p>
        <p>профиль: <?=$tire['profile']?></p>
        <p>ширина: <?=$tire['width']?></p>
        <p>сезон: <?=$seasons[$tire['season']]?></p>
        <img src="<?=ROOT?>img/other/<?=$tire['src']?>" width="100">
        <h3>галерея</h3>
        <?php foreach ($gallery as $image): ?>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <img src="<?=ROOT?>img/other/<?=$image['src']?>" width="300">
            </div>
        <?php endforeach; ?>
	</div>
</div>