<?php
$r1 = mysqli_query($db, "SELECT b.class, b.header AS carousel_header, b.type, b.is_header, c.id, c.name, c.description, c.link, c.is_lightbox, c.photo
	FROM carousels_blocks AS b, carousels AS c
	WHERE b.group_id = '$block_id' AND c.block_id =  b.group_id 
	AND c.language_id = '".$_SESSION['lang']."'
	AND b.language_id = '".$_SESSION['lang']."'
	ORDER BY c.sort ASC");

include 'languages/'.$_SESSION['lang'].'/language.php';
$header = false;
$carousel_type = 0;
for ($i = 1; $f1 = mysqli_fetch_assoc($r1); $i++) {
    $carousel_type = $f1['type'];
	$class = $f1['class'];?>

		<?php
		$header = true;
		switch ($f1['type']){
            case '1':
                if ($i == 1){
                    if ($f1['is_header']) {
                        echo "<div class='section-title text-center'>
                                <h2>{$f1['carousel_header']}</h2>
                             </div>";
                    }
                    echo "
                    <div class=\"row expanded align-center\">
                        <div class=\"small-12 large-10 columns\">
                            <div class=\"center-slider offset-slider case-1\">"
                    ;
                }
                ?>
                            <div class="slide-item">
                                <div style="background: url(<?=ROOT?>img/other/<?=$f1['photo']?>) 50% 50%/cover no-repeat" class="image-holder has-background"></div>
                            </div>

<?php
                break;


            case '2':
                if ($i == 1){
                    if ($f1['is_header']) {
                        echo "<div class='section-title text-center'>
                                <h2>{$f1['carousel_header']}</h2>
                             </div>";
                    }
                    echo "
                    <div class=\"row expanded content-offset\">
                        <div class=\"small-12 columns no-padding\">
                            <div class=\"offset-slider double-slider left-controls\">"
                    ;
                }
                ?>
                            <div class="slide-item full-height">
                                <div style="background-image: url(<?=ROOT?>img/other/<?=$f1['photo']?>)" class="image-holder has-background"></div>
                            </div>

<?php
                break;
            case '3':
                if ($i == 1){
                    if ($f1['is_header']) {
                        echo "<div class='section-title text-center'>
                                <h2>{$f1['carousel_header']}</h2>
                             </div>";
                    }
                    $carousel_text = '';
                    echo "                    
                        <div class=\"projects-slider-wrapper\">
                            <div class=\"offset-slider left-side projects-slider\">"
                    ;
                }

                $carousel_text .= "
                    <div class=\"b-info_item\">
                        <div class=\"title\">
                            <h3>{$f1['name']}</h3>
                        </div>
                        <div class=\"content\">
                            <p>" . strip_tags(htmlspecialchars_decode($f1['description'])) . "</p>
                        </div><a href=\"{$f1['link']}\" class=\"button\">" . $lang['read_more'] . "</a>
                    </div>";
                ?>

                <div class="slide-item full-height">
                    <div style="background-image: url(<?=ROOT?>img/other/<?=$f1['photo']?>)" class="image-holder has-background"></div>
                </div>

<?php
                break;

            case '4':
                if ($i == 1){
                    echo "
                        <section class=\"section-offset\">
                          <div class=\"row align-center\">";
                    if ($f1['is_header']) {
                        echo "<div class='section-title text-center'>
                                <h2>{$f1['carousel_header']}</h2>
                             </div>";
                    }
                    echo "<div class=\"small-12 large-10 columns\">
                              <div class=\"home-verical-slider\">";
                }
                ?>
                <div class="slide-item">
                    <div class="row">
                        <div class="small-12 medium-4 columns content-holder">
                            <div class="b-info_item case-1">
                                <div class="title">
                                    <h3><?=$f1['name']?></h3>
                                </div>
                                <div class="content">
                                    <p>
                                        <?=htmlspecialchars_decode($f1['description'])?>
                                    </p>
                                </div>
                                <?php /*if ($f1['link']){*/
                                    echo "<a href=\"/pages/Our-projects\" class=\"button\">VIEW ALL PROJECTS</a>";
                                /*}*/ ?>
                            </div>
                        </div>
                        <div class="small-12 medium-8 columns image-holder">
                            <img style="max-width: 100%" src="<?=ROOT?>img/other/<?=$f1['photo']?>" alt="pic">
                        </div>
                    </div>
                </div>

<?php
                break;
            case '5':
                if ($i == 1){
                    if ($f1['is_header']) {
                        echo "<div class='section-title text-center'>
                                <h2>{$f1['carousel_header']}</h2>
                             </div>";
                    }
                    echo "
                    <div class=\"row expanded content-offset\">
                        <div class=\"small-12 columns no-padding\">
                            <div class=\"offset-slider double-slider right-controls\">"
                    ;
                }
                ?>
                <div class="slide-item full-height">
                    <div style="background-image: url(<?=ROOT?>img/other/<?=$f1['photo']?>)" class="image-holder has-background"></div>
                </div>

                <?php
                break;
        }
}
switch ($carousel_type){
    case '1':
        echo "
                        </div>
                    </div>
                </div>"
        ;
        break;

    case '2':
        echo "
                        </div>
                    </div>
                </div>"
        ;
        break;

    case '3':
            echo  "</div>";
        echo "<div class=\"projects-slider-info\">";
        echo $carousel_text;
        echo "</div>
        </div>";
        break;

    case '4':
        echo "</div>
            </div>
          </div>
        </section>";
        break;

    case '5':
        echo "
                        </div>
                    </div>
                </div>"
        ;
        break;
}
?>