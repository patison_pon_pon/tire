<?php
function drawMenu($position){
	global $db;
	global $root;
	$r = mysqli_query($db, "SELECT * FROM menus WHERE position = '$position'");
	while ($f = mysqli_fetch_assoc($r)) {
		$menu_id = $f['group_id'];
		$r1 = mysqli_query($db, "SELECT i.page_id, p.id, p.alias, p.name FROM menus_items AS i, pages AS p WHERE i.page_id = p.group_id AND i.parent = '0' AND i.menu_id = '$menu_id' AND p.language_id = '".$_SESSION['lang']."' ORDER BY i.sort");
		while ($f1 = mysqli_fetch_assoc($r1)) {
			$menu[$f1['id']] = $f1;
			$pid = $f1['id'];
			$r2 = mysqli_query($db, "SELECT i.page_id, p.id, p.alias, p.name FROM menus_items AS i, pages AS p WHERE i.page_id = p.id AND i.parent = '$pid' AND i.menu_id = '$menu_id' ORDER BY i.sort");
			if ($r2) {
				while ($f2 = mysqli_fetch_assoc($r2)) {
					$menu[$f1['id']]['sons'][$f2['id']] = $f2;
				}
			}
		}
		switch ($f['type']) {
			case 'horizontal':
		?>
        <div class="<?=$f['class']?>">
            <div class="site-logo">
            </div>
            <nav class="site-nav">
                <div class="site-nav_inner">
                    <?php
                    foreach ($menu as $f) {
                        echo "<a href='";
                            if ($f['id'] == '1') {
                                echo $root;
                            }else{
                                echo "{$root}pages/{$f['alias']}";
                            }
                        echo "'>{$f['name']}</a>";
                    }
                    ?>
				</div>
			</nav>
            <div class="menu-button">
                <div class="sandwich">
                    <div class="sw-topper"></div>
                    <div class="sw-bottom"></div>
                    <div class="sw-footer"></div>
                </div>
            </div>
        </div>
<?php
			break;
			
			case 'vertical':?>

                <ul class="column-count">
                    <?php
                    foreach ($menu as $f) {
                            echo "<li><a href='";
                                        if ($f['id'] == '1') {
                                            echo $root;
                                        }else{
                                            echo "{$root}pages/{$f['alias']}";
                                        }
                                    echo "'>{$f['name']}</a></li>";
                    }
                    ?>
                </ul>
<?php
				break;
		}
	}
}
?>