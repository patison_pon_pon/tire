<div class="row align-center">
    <div class="small-12 large-10 columns">
<?php
$r = mysqli_query($db, "SELECT * FROM $type WHERE parent = '$id' AND language_id = '".$_SESSION['lang']."' ORDER BY sort");
while ($f = mysqli_fetch_assoc($r)) {
	$sons[] = $f;
}
if ($sons) {
	echo "<div class='posts sons post-group-flex'>";
	foreach ($sons as $s) {
		if ($myBase['is_text_prewie'] && $myBase['is_image_prewie']) {?>
			<article class="post sons_t1_i1" tabindex="0">
				<a href="<?=ROOT?>pages/<?=$s['alias']?>"><div class="post-img" style="background: url(<?=ROOT?>img/other/<?=$s['photo']?>) 50% 50%/cover no-repeat;"></div></a>
				
				<div class="post-content">
					<h4 class="text-left"><?=$s['name']?></h4>
					<?=short(strip_tags(htmlspecialchars_decode($s['description'])), 300)?><br><br>
					<a href="<?=ROOT?>pages/<?=$s['alias']?>" class="post-detail"><?=$lang['read_more']?>&nbsp;<img src="<?=ROOT?>assets/images/strelka.svg"></a>
				</div>
			</article>
<?php
		}
		elseif ($myBase['is_text_prewie'] == 1 && !$myBase['is_image_prewie']) {?>
			<article class="post sons_t1_i0" tabindex="0">
				<a href="<?=ROOT?>pages/<?=$s['alias']?>"><div class="post-img"></div></a>
				
				<div class="post-content">
					<h4 class="text-left"><?=$s['name']?></h4>
					<?=short(strip_tags(htmlspecialchars_decode($s['description'])), 300)?><br><br>
					<a href="<?=ROOT?>pages/<?=$s['alias']?>" class="post-detail"><?=$lang['read_more']?>&nbsp;<img src="<?=ROOT?>assets/images/strelka.svg"></a>
				</div>
			</article>
<?php
		}
		elseif (!$myBase['is_text_prewie'] && $myBase['is_image_prewie']) {?>
			<article class="post sons_t0_i1" tabindex="0">
				<a href="<?=ROOT?>pages/<?=$s['alias']?>"><div class="post-img" style="background: url(<?=ROOT?>img/other/<?=$s['photo']?>) 50% 50%/cover no-repeat;"></div></a>
				
				<div class="post-content">
					<h4 class="text-left"><?=$s['name']?></h4>
					<a href="<?=ROOT?>pages/<?=$s['alias']?>" class="post-detail"><?=$lang['read_more']?>&nbsp;<img src="<?=ROOT?>assets/images/strelka.svg"></a>
				</div>
			</article>
<?php
		}
	}
	echo "</div>";
}
?>
    </div>
</div>