-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Июн 01 2020 г., 13:41
-- Версия сервера: 5.7.30-0ubuntu0.18.04.1
-- Версия PHP: 7.0.33-29+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `tire`
--

-- --------------------------------------------------------

--
-- Структура таблицы `administrators`
--

CREATE TABLE `administrators` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `login` varchar(255) CHARACTER SET utf8 NOT NULL,
  `password` varchar(32) CHARACTER SET utf8 NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `administrators`
--

INSERT INTO `administrators` (`id`, `name`, `language_id`, `login`, `password`, `date_add`) VALUES
(3, 'Главный администратор', 1, 'admin', 'b4e58e198edeb686626a81ccb590373c', '0000-00-00 00:00:00'),
(4, 'user', 1, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', '2016-04-19 00:00:00'),
(5, 'admin', 1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '2016-11-27 19:02:56'),
(6, 'admin', 1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '2016-11-27 19:03:43');

-- --------------------------------------------------------

--
-- Структура таблицы `carousels`
--

CREATE TABLE `carousels` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(11) NOT NULL,
  `name` text CHARACTER SET utf8 NOT NULL COMMENT 'Заголовок',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `description` varchar(255) CHARACTER SET utf8 NOT NULL,
  `link` text CHARACTER SET utf8 NOT NULL COMMENT 'Метатитлы',
  `photo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `sort` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `carousels_blocks`
--

CREATE TABLE `carousels_blocks` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `class` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_header` int(1) NOT NULL DEFAULT '1',
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `galerys`
--

CREATE TABLE `galerys` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `galerys_photo`
--

CREATE TABLE `galerys_photo` (
  `photo_id` int(11) NOT NULL,
  `photo_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `photo_desc` varchar(255) CHARACTER SET utf8 NOT NULL,
  `src` varchar(255) CHARACTER SET utf8 NOT NULL,
  `pp_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `htmls`
--

CREATE TABLE `htmls` (
  `id` int(11) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `full_text` longtext CHARACTER SET utf8 NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `htmls`
--

INSERT INTO `htmls` (`id`, `group_id`, `name`, `alias`, `language_id`, `full_text`, `date_add`) VALUES
(1, 1, 'Фильтр', 'Filter', 1, '<div class=\"tire-filter-container\"></div>', '2020-05-29 00:00:00'),
(2, 2, 'Каталог', 'Catalog', 1, '<div class=\"tire-catalog-container\"></div>', '2020-05-29 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `languages`
--

CREATE TABLE `languages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `languages`
--

INSERT INTO `languages` (`id`, `name`, `language_id`) VALUES
(1, 'Русский', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `class` varchar(255) CHARACTER SET utf8 NOT NULL,
  `position` int(11) NOT NULL,
  `is_header` int(11) NOT NULL,
  `date_add` datetime NOT NULL,
  `type` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `menus`
--

INSERT INTO `menus` (`id`, `group_id`, `name`, `alias`, `language_id`, `class`, `position`, `is_header`, `date_add`, `type`) VALUES
(1, 1, 'Главное меню', 'glavnoe-menyu', 1, 'main-menu', 1, 1, '2020-05-29 00:00:00', 'horizontal');

-- --------------------------------------------------------

--
-- Структура таблицы `menus_items`
--

CREATE TABLE `menus_items` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `parent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `menus_items`
--

INSERT INTO `menus_items` (`id`, `menu_id`, `page_id`, `sort`, `parent`) VALUES
(1, 1, 1, 10, 0),
(2, 1, 658, 20, 0),
(3, 1, 659, 30, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `menus_positions`
--

CREATE TABLE `menus_positions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `menus_positions`
--

INSERT INTO `menus_positions` (`id`, `name`) VALUES
(1, 'Главное меню'),
(2, 'Левая колонка'),
(3, 'Меню футер'),
(4, 'Футер 1 колонка'),
(5, 'Футер 2 колонка'),
(6, 'Футер 3 колонка'),
(7, 'Футер 4 колонка');

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(11) NOT NULL DEFAULT '0',
  `alias` text CHARACTER SET utf8 NOT NULL COMMENT 'Персональный URL',
  `name` text CHARACTER SET utf8 NOT NULL COMMENT 'Заголовок',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `description` text CHARACTER SET utf8,
  `full_text` longtext CHARACTER SET utf8 COMMENT 'Основной текст',
  `meta_t` text CHARACTER SET utf8 COMMENT 'Метатитлы',
  `meta_k` text CHARACTER SET utf8 COMMENT 'Ключевые слова (keywords)',
  `meta_d` text COMMENT 'Описание (description)',
  `date_add` datetime NOT NULL,
  `leftbar` int(11) NOT NULL DEFAULT '0',
  `parent` int(11) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '10',
  `photo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `is_text_prewie` int(1) DEFAULT '0',
  `is_image_prewie` int(1) NOT NULL DEFAULT '1',
  `is_rewies` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `group_id`, `alias`, `name`, `language_id`, `description`, `full_text`, `meta_t`, `meta_k`, `meta_d`, `date_add`, `leftbar`, `parent`, `sort`, `photo`, `is_text_prewie`, `is_image_prewie`, `is_rewies`) VALUES
(1, 1, 'home', 'Главная', 1, '', '', 'Главная', '', 'Деск', '2016-05-12 00:00:00', 0, 0, 20, '', 0, 0, 0),
(658, 0, '111', '111', 1, NULL, NULL, NULL, NULL, NULL, '2020-06-01 12:31:54', 0, 0, 10, NULL, 0, 1, 0),
(659, 659, '222', '222', 1, NULL, NULL, NULL, NULL, NULL, '2020-06-01 12:34:23', 0, 0, 10, NULL, 0, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `pages_wrap`
--

CREATE TABLE `pages_wrap` (
  `id` int(11) NOT NULL,
  `page_id` varchar(255) CHARACTER SET utf8 NOT NULL,
  `table_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `block_id` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `pages_wrap`
--

INSERT INTO `pages_wrap` (`id`, `page_id`, `table_name`, `block_id`, `position`, `sort`) VALUES
(566, '1', 'htmls', 2, 2, 20),
(567, '1', 'htmls', 1, 2, 10);

-- --------------------------------------------------------

--
-- Структура таблицы `positions`
--

CREATE TABLE `positions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `positions`
--

INSERT INTO `positions` (`id`, `name`) VALUES
(1, 'Верх страницы'),
(2, 'Содержание верх'),
(3, 'Содержание низ'),
(4, 'Низ страницы'),
(5, 'Левая колонка'),
(6, 'Хедер'),
(7, 'Футер 1 колонка'),
(8, 'Футер 2 колонка'),
(9, 'Футер 3 колонка'),
(10, 'Футер 4 колонка'),
(11, 'Футер низ');

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(11) NOT NULL,
  `alias` text CHARACTER SET utf8 NOT NULL COMMENT 'Персональный URL',
  `name` text CHARACTER SET utf8 NOT NULL COMMENT 'Заголовок',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `description` text CHARACTER SET utf8 NOT NULL,
  `full_text` longtext CHARACTER SET utf8 NOT NULL COMMENT 'Основной текст',
  `meta_t` text CHARACTER SET utf8 NOT NULL COMMENT 'Метатитлы',
  `meta_k` text CHARACTER SET utf8 NOT NULL COMMENT 'Ключевые слова (keywords)',
  `meta_d` text CHARACTER SET utf8 NOT NULL COMMENT 'Описание (description)',
  `photo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `block_id` int(11) NOT NULL,
  `leftbar` int(1) NOT NULL DEFAULT '1',
  `date_add` datetime NOT NULL,
  `sort` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `posts_blocks`
--

CREATE TABLE `posts_blocks` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `meta_t` varchar(255) CHARACTER SET utf8 NOT NULL,
  `meta_d` varchar(255) CHARACTER SET utf8 NOT NULL,
  `meta_k` varchar(255) CHARACTER SET utf8 NOT NULL,
  `class` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_header` int(1) NOT NULL DEFAULT '1',
  `is_text_prewie` int(1) NOT NULL DEFAULT '1',
  `is_image_prewie` int(1) NOT NULL DEFAULT '1',
  `quantity` int(11) NOT NULL DEFAULT '5',
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `rewies`
--

CREATE TABLE `rewies` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `customer_id` int(11) NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8 NOT NULL,
  `full_text` text CHARACTER SET utf8 NOT NULL,
  `photo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `rating` int(11) NOT NULL,
  `date_add` datetime NOT NULL,
  `checked` int(11) NOT NULL DEFAULT '0',
  `type_from` varchar(255) CHARACTER SET utf8 NOT NULL,
  `group_id` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `sliders_photo`
--

CREATE TABLE `sliders_photo` (
  `photo_id` int(11) NOT NULL,
  `photo_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `photo_desc` varchar(255) CHARACTER SET utf8 NOT NULL,
  `src` varchar(255) CHARACTER SET utf8 NOT NULL,
  `pp_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `tire`
--

CREATE TABLE `tire` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) DEFAULT '1',
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  `diameter_id` int(11) DEFAULT NULL,
  `profile_id` int(11) DEFAULT NULL,
  `width_id` int(11) DEFAULT NULL,
  `season` int(11) DEFAULT NULL,
  `description` text,
  `date_add` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tire`
--

INSERT INTO `tire` (`id`, `group_id`, `language_id`, `name`, `alias`, `manufacturer_id`, `diameter_id`, `profile_id`, `width_id`, `season`, `description`, `date_add`) VALUES
(1, 1, 1, '111', '111', 6, 1, 3, 1, 2, NULL, '2020-05-29'),
(2, 2, 1, '2', '2', 6, 1, 1, 4, 2, NULL, '2020-06-01');

-- --------------------------------------------------------

--
-- Структура таблицы `tire_diameter`
--

CREATE TABLE `tire_diameter` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `date_add` date NOT NULL,
  `language_id` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tire_diameter`
--

INSERT INTO `tire_diameter` (`id`, `group_id`, `name`, `alias`, `date_add`, `language_id`) VALUES
(1, 1, 'R10', 'R10', '2020-05-28', 1),
(2, 2, 'R11', 'R11', '2020-05-28', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tire_manufacturer`
--

CREATE TABLE `tire_manufacturer` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) DEFAULT '1',
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `date_add` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tire_manufacturer`
--

INSERT INTO `tire_manufacturer` (`id`, `group_id`, `language_id`, `name`, `alias`, `date_add`) VALUES
(1, 1, 1, 'TOYO', 'TOYO', '2020-05-28'),
(6, 6, 1, 'Rosava', 'Rosava', '2020-05-28'),
(8, 8, 1, '333', '333', '2020-05-28');

-- --------------------------------------------------------

--
-- Структура таблицы `tire_photo`
--

CREATE TABLE `tire_photo` (
  `id` int(11) NOT NULL,
  `photo_name` char(255) DEFAULT NULL,
  `photo_desc` text,
  `src` char(255) NOT NULL,
  `pp_id` int(11) DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '10'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tire_photo`
--

INSERT INTO `tire_photo` (`id`, `photo_name`, `photo_desc`, `src`, `pp_id`, `sort`) VALUES
(1, '', '', '1591001730.jpg', 1, 10),
(2, '', '', '1591001772.jpg', 1, 10),
(3, '', '', '1591001779.jpg', 1, 10),
(4, '', '', '1591001805.jpg', 1, 10),
(5, '', '', '1591001907.jpg', 2, 20),
(6, '', '', '1591001913.jpg', 2, 10),
(7, '', '', '1591001952.jpg', 2, 30);

-- --------------------------------------------------------

--
-- Структура таблицы `tire_profile`
--

CREATE TABLE `tire_profile` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) DEFAULT '1',
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `date_add` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tire_profile`
--

INSERT INTO `tire_profile` (`id`, `group_id`, `language_id`, `name`, `alias`, `date_add`) VALUES
(1, 1, 1, '40', '40', '2020-05-28'),
(2, 2, 1, '50', '50', '2020-05-28'),
(3, 3, 1, '60', '60', '2020-05-28'),
(4, 4, 1, '70', '70', '2020-05-28'),
(5, 5, 1, '80', '80', '2020-05-28');

-- --------------------------------------------------------

--
-- Структура таблицы `tire_width`
--

CREATE TABLE `tire_width` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `date_add` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tire_width`
--

INSERT INTO `tire_width` (`id`, `group_id`, `language_id`, `name`, `alias`, `date_add`) VALUES
(1, 1, 1, '255', '255', '2020-05-28'),
(2, 2, 1, '250', '250', '2020-06-01'),
(3, 3, 1, '245', '245', '2020-06-01'),
(4, 4, 1, '240', '240', '2020-06-01');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `administrators`
--
ALTER TABLE `administrators`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `carousels`
--
ALTER TABLE `carousels`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `carousels_blocks`
--
ALTER TABLE `carousels_blocks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `galerys`
--
ALTER TABLE `galerys`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `galerys_photo`
--
ALTER TABLE `galerys_photo`
  ADD PRIMARY KEY (`photo_id`);

--
-- Индексы таблицы `htmls`
--
ALTER TABLE `htmls`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menus_items`
--
ALTER TABLE `menus_items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menus_positions`
--
ALTER TABLE `menus_positions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pages_wrap`
--
ALTER TABLE `pages_wrap`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `posts_blocks`
--
ALTER TABLE `posts_blocks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `rewies`
--
ALTER TABLE `rewies`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sliders_photo`
--
ALTER TABLE `sliders_photo`
  ADD PRIMARY KEY (`photo_id`);

--
-- Индексы таблицы `tire`
--
ALTER TABLE `tire`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tire_diameter_id_fk` (`diameter_id`),
  ADD KEY `tire_manufacturer_id_fk` (`manufacturer_id`),
  ADD KEY `tire_profile_id_fk` (`profile_id`),
  ADD KEY `tire_width_id_fk` (`width_id`);

--
-- Индексы таблицы `tire_diameter`
--
ALTER TABLE `tire_diameter`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tire_manufacturer`
--
ALTER TABLE `tire_manufacturer`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tire_photo`
--
ALTER TABLE `tire_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tire_photo_pp_id_fk` (`pp_id`);

--
-- Индексы таблицы `tire_profile`
--
ALTER TABLE `tire_profile`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tire_width`
--
ALTER TABLE `tire_width`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `administrators`
--
ALTER TABLE `administrators`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `carousels`
--
ALTER TABLE `carousels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `carousels_blocks`
--
ALTER TABLE `carousels_blocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `galerys`
--
ALTER TABLE `galerys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `galerys_photo`
--
ALTER TABLE `galerys_photo`
  MODIFY `photo_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `htmls`
--
ALTER TABLE `htmls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `menus_items`
--
ALTER TABLE `menus_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `menus_positions`
--
ALTER TABLE `menus_positions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=660;

--
-- AUTO_INCREMENT для таблицы `pages_wrap`
--
ALTER TABLE `pages_wrap`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=568;

--
-- AUTO_INCREMENT для таблицы `positions`
--
ALTER TABLE `positions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `posts_blocks`
--
ALTER TABLE `posts_blocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `rewies`
--
ALTER TABLE `rewies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `sliders_photo`
--
ALTER TABLE `sliders_photo`
  MODIFY `photo_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tire`
--
ALTER TABLE `tire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `tire_diameter`
--
ALTER TABLE `tire_diameter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `tire_manufacturer`
--
ALTER TABLE `tire_manufacturer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `tire_photo`
--
ALTER TABLE `tire_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `tire_profile`
--
ALTER TABLE `tire_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `tire_width`
--
ALTER TABLE `tire_width`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `tire`
--
ALTER TABLE `tire`
  ADD CONSTRAINT `tire_diameter_id_fk` FOREIGN KEY (`diameter_id`) REFERENCES `tire_diameter` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tire_manufacturer_id_fk` FOREIGN KEY (`manufacturer_id`) REFERENCES `tire_manufacturer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tire_profile_id_fk` FOREIGN KEY (`profile_id`) REFERENCES `tire_profile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tire_width_id_fk` FOREIGN KEY (`width_id`) REFERENCES `tire_width` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tire_photo`
--
ALTER TABLE `tire_photo`
  ADD CONSTRAINT `tire_photo_pp_id_fk` FOREIGN KEY (`pp_id`) REFERENCES `tire` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
