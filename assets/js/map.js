ymaps.ready(function () {
        
        var myMap = new ymaps.Map('map', {
            center: [61.270481, 73.377524],
            zoom: 16,
            controls: ['zoomControl' ],

        }, {
            searchControlProvider: 'yandex#search'
            
        });
    
        myGeoObject = new ymaps.GeoObject({
            // Описание геометрии.
            
            // Свойства.
            
        }, {
            // Опции.
            // Иконка метки будет растягиваться под размер ее содержимого.
            preset: 'islands#blackStretchyIcon',
            // Метку можно перемещать.
            draggable: true
        });
        myPlacemark = new ymaps.Placemark([61.270481, 73.377524], {
            hintContent: 'г. Сургут, Профсоюзов 9/1, 628400',
            // balloonContent: 'Офис IT86'
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: '/images/mark.png',
            // Размеры метки.
            iconImageSize: [58, 78],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-29, -78]
        });

    myMap.geoObjects.add(myPlacemark);
        
        // myMap.geoObjects
        // .add(myGeoObject)
        // .add(new ymaps.Placemark([61.24971625069299,73.41908350000001], {
            
        // }));
        myMap.behaviors.disable('scrollZoom');
});
