const limit = 10;

$(document).ready(function () {
    const url = "/ajax/tire_filter.php";
    let filterContainer = $('.tire-filter-container');
    let catalogContainer = $('.tire-catalog-container');

    $.ajax({
        url: url,
        cache: false,
        type: "POST",
        data: {action: 'draw_filter'},
        success: function (data) {
            filterContainer.html(data)
        },
        error: function (data) {
            console.log(data);
        }
    });

    function goFilter(request){
        request.action = 'go_filter';
        $.ajax({
            url: url,
            cache: false,
            type: "POST",
            data: request,
            success: function (data) {
                catalogContainer.html(data)
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    $(document).on('submit','form.tire-filter-form',function(event){
        event.preventDefault();
        let request = {};
        $.each($(this).serializeArray(), function (key, value) {
            request[value.name] = value.value;
        });
        page = 1;
        request.limit = limit;
        request.page = page;
        goFilter(request)

    });

    $(document).on('click','#paginator button',function(event){
        event.preventDefault();
        let request = {};
        $.each($(this).serializeArray(), function (key, value) {
            request[value.name] = value.value;
        });
        page = 1;
        request.limit = limit;
        request.page = $(this).data('page');
        goFilter(request)

    });
})