  var payHandler = function () {
      //требуется библиотека jquery
      var widget = new cp.CloudPayments();
      var data = {
          //данные
          name: $('.victim #name').val(),
          email: $('.victim #email').val(),
          phone: $('.victim #phone').val()

      };

      var name = $('.victim #name').val();
      var donat = parseInt($('.victim #donat').val());
      var email = $('.victim #email').val();

      //проверка заполнения полей
      if (!name || name == '' || name == undefined || name == 'undefined') {
          alert('Необходимо ввести Имя')
      } else if (!email || email == '' || email == undefined || email == 'undefined') {
          alert('Необходимо ввести Email')
      } else if (!donat || donat === '' || donat === undefined || donat === 'undefined' || donat === 0 || donat === NaN || donat === 'NaN') {
          alert('Необходимо ввести сумму');
      } else {

          widget.charge({ // options
                  publicId: 'pk_ac28897ec17d19ffdbca83ee167f9',
                  description: 'Оплата у нас на сайте',
                  amount: donat, //сумма
                  currency: 'RUB',
                  email: email,
                  invoiceId: 'Оплата аренда авто',
                  accountId: email, //плательщик
                  data: data
              },
              function (options) { // success
                  //действие при успешном платеже
              },
              function (reason, options) { // fail
                  //действие при неуспешном платеже
              });
      }
  };
  $("#button").on("click", payHandler); //кнопка "Оплатить" 