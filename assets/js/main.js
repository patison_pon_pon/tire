$(function() {

$('.hamburger').click(function(){

	if ($('body').hasClass('scrolldisable')) {
		$('body, html').removeClass('scrolldisable');
		$('.mobile-nav').removeClass('open');
		$('.hamburger').removeClass('is-active');
	} else {
		$('body, html').addClass('scrolldisable');
		$('.mobile-nav').addClass('open');
		$('.hamburger').addClass('is-active');
	}
	
});

document.ontouchmove = function(event){
	if ($('body').hasClass('scrolldisable')) {
		event.preventDefault();
	} else {
		
	}
}

// if ($(window).width() > 720) {
// 	$('body').append(
// 		'<link rel="stylesheet" href="https://cdn.envybox.io/widget/cbk.css">'+
// 		'<script type="text/javascript" src="https://cdn.envybox.io/widget/cbk.js?wcb_code=574bcde8a9c6af3239ccc2282185e7d6" charset="UTF-8" async></script>'
// 	)
// }

$(document).on('click', '.like-button', function(){
	if (!($(this).hasClass('selected'))) {
		$(this).next('.dislike-button').removeClass('selected');
		$(this).addClass('selected');
		likeDislike($(this).data('reviews-id'), 'plus');
	}
});

$(document).on('click', '.dislike-button', function(){
	if (!($(this).hasClass('selected'))) {
		$(this).prev('.like-button').removeClass('selected');
		$(this).addClass('selected');
		likeDislike($(this).data('reviews-id'), 'minus');
	}
});

function likeDislike(id, action){
	$.ajax({
		type: 'post',
		url: '/site/like-dislike',
		data: {
			id : id,
			action: action
		},
		dataType: 'json',
		success: function(data){
			$('.numb_'+ id).removeClass('plus minus neutral');

			if (data.class == 'plus') {
				$('.numb_'+ id).html('+ '+ data.difference).addClass('plus');
			} else if (data.class == 'minus') {
				$('.numb_'+ id).html('- '+ data.difference).addClass('minus');
			} else {
				$('.numb_'+ id).html(data.difference).addClass('neutral');
			}
		}
	})
}

$('.add-review-button').click(function(){
	$('.review-form-wrapper').slideToggle();
});

$('.car-cat-selector').click(function(){
	if (!($(this).hasClass('active'))) {
		var index = $(this).index();
		$('.car-cat-selector').not(this).removeClass('active');
		$(this).addClass('active');
		$('.car-info-block').hide();
		$('.car-info-block').eq(index).fadeIn();
	}
});

$(".phone").mask("+7 (999) 999-99-99");

jQuery.datetimepicker.setLocale('ru');
$('.datepicker').datetimepicker({
	defaultDate: '01.01.1990',
	format: 'd.m.Y',
	timepicker: false
});

$('.form-selector').click(function(){
	if (!($(this).hasClass('active'))) {
		$('.form-selector').removeClass('active');
		$(this).addClass('active');
		$('.education').val($(this).html());
	}
});

$('.find').on('click', function(){
	$('.find_another').val($(this).parent().find('label').data('value'));
});
$('.rental').on('click', function(){
	$('.rental_companies').val($(this).parent().find('label').data('value'));
});

//$('.security-form').on('submit', function(e){
//	var _this = $(this);
//	e.preventDefault();
//	$.ajax({
//		type: 'post',
//		url: _this.prop('action'),
//		data: _this.serialize(),
//		dataType: 'json',
//		success: function(data){
//
//		}
//	})
//});

$('.car-cat').each(function(i,val){
	if ($(val).hasClass('active')) {
		$(val).parent().find('.float-block').css({
			'left': $(val).position().left,
			'top': $(val).position().top,
			'width': $(val).innerWidth()
		});
	}
});

$('.car-cat').click(function(){
	var pos   = $(this).position().left,
		top   = $(this).position().top,
		width = $(this).innerWidth();
	$(this).parent().find('.car-cat').removeClass('active');
	$(this).addClass('active');

	$('.reason_absence').val($(this).data('value'));

	if ($(this).data('type-id') != undefined) {
		$('.type_auto').val($(this).data('type-id'));
	}
	$('#search_form_for_car').submit();
	$(this).parent().find('.float-block').css({
		'left': pos,
		'top': top,
		'width': width
	})
});

$('#search_form_for_car').submit(function(e){
	var _this = $(this);
	e.preventDefault();
	History.pushState(null, null, _this.prop('action').split('?')[0] +'?'+ _this.serialize());
	$.ajax({
		type: 'get',
		url: _this.prop('action').split('?')[0],
		data: _this.serialize(),
		dataType: 'json',
		success: function(data){
			$('.block_cars').html(data)
		}
	});
});
$('.page-upper').click(function(){
	$('html, body').animate({
		scrollTop: 0,
	}, 800)
})

// Подключение Slick Carousel


if ($(document).width() <= 1271) {
	$('.tariffs').slick({
		dots: false,
		arrows: true,
		infinite: false,
		speed: 800,
		slidesToShow: 3,
		slidesToScroll: 1,
		responsive: [
		    {
				breakpoint: 720,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
				}
		    },
		    {
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				}
		    }
	  	]
	});
}

$('.car-slider').slick({
	dots: false,
	arrows: true,
	infinite: true,
	speed: 800,
	slidesToShow: 1,
	slidesToScroll: 1,
});
if ($(document).width() >= 1271) {
	$('.car-gallery-slider').slick({
		dots: false,
		arrows: true,
		infinite: true,
		speed: 800,
		slidesToShow: 6,
		slidesToScroll: 1
     
	});
	$('.hotel-gallery-slider').slick({
		dots: false,
		arrows: true,
		infinite: true,
		speed: 800,
		slidesToShow: 6,
		slidesToScroll: 1
	});
	$('.popular-car-slider').slick({
		dots: false,
		arrows: false,
		infinite: true,
		speed: 800,
		slidesToShow: 4,
		slidesToScroll: 1,


                    autoplay: true,
                           autoplaySpeed: 3000

	});
} else {
	$('.car-gallery-slider').slick({
		dots: false,
		arrows: true,
		infinite: true,
		speed: 800,
		slidesToShow: 6,
		slidesToScroll: 1,
		responsive: [
		    {
				breakpoint: 1239,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 4,
					arrows: false,
					dots: true,
				}
		    },
		    {
				breakpoint: 800,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					arrows: false,
					dots: true,
				}
		    },
		    {
				breakpoint: 480,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					arrows: false,
					dots: true,
				}
		    }
		]
	});
	$('.hotel-gallery-slider').slick({
		dots: false,
		arrows: true,
		infinite: true,
		speed: 800,
		slidesToShow: 6,
		slidesToScroll: 1,
		responsive: [
		    {
				breakpoint: 1239,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 4,
					arrows: false,
					dots: true,
				}
		    },
		    {
				breakpoint: 800,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					arrows: false,
					dots: true,
				}
		    },
		    {
				breakpoint: 480,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					arrows: false,
					dots: true,
				}
		    }
		]
	});
	$('.popular-car-slider').slick({
		dots: false,
		arrows: false,
		infinite: true,
		speed: 800,
		slidesToShow: 4,
		slidesToScroll: 1,
		responsive: [
		    {
				breakpoint: 1240,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
				}
		    },
		    {
				breakpoint: 980,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
				}
		    },
		    {
				breakpoint: 640,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				}
		    }
	  	]
	});
}
$('.index-banner-slider').slick({
	dots: true,
	arrows: false,
	infinite: true,
	speed: 800,
	slidesToShow: 1,
	slidesToScroll: 1,
 
                    autoplay: true,
                           autoplaySpeed: 3000
});
$('.popular-car-slider-prev').click(function(){
	$('.popular-car-slider').slick('slickPrev');
});
$('.popular-car-slider-next').click(function(){
	$('.popular-car-slider').slick('slickNext');
});
$('.reviews-slider').slick({
	dots: false,
	arrows: false,
	infinite: true,
	speed: 800,
	slidesToShow: 1,
	slidesToScroll: 1,
	adaptiveHeight: true
});
$('.reviews-slider-prev').click(function(){
	$('.reviews-slider').slick('slickPrev');
});
$('.reviews-slider-next').click(function(){
	$('.reviews-slider').slick('slickNext');
})
// 

// Подключение Magnific Gallery
$('.zoom-gallery').magnificPopup({
	delegate: 'a',
	type: 'image',
	closeOnContentClick: false,
	closeBtnInside: false,
	mainClass: 'mfp-with-zoom mfp-img-mobile',
	image: {
		verticalFit: true,
		// titleSrc: function(item) {
		// 	return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
		// }
	},
	gallery: {
		enabled: true
	},
	zoom: {
		enabled: true,
		duration: 300, // don't foget to change the duration also in CSS
		opener: function(element) {
			return element.find('img');
		}
	}
});
// 

// Подключение Magnific Modal
$('.popup').magnificPopup({
	type: 'inline',

	fixedContentPos: false,
	fixedBgPos: true,

	overflowY: 'auto',

	closeBtnInside: true,
	preloader: false,
	
	midClick: true,
	removalDelay: 300,
	mainClass: 'my-mfp-zoom-in'
});
	var auto_id;
$('.popup-auto').on('click', function(){
	auto_id = $(this).data('auto-id');
});
$('.popup-auto').magnificPopup({
	type: 'inline',

	fixedContentPos: false,
	fixedBgPos: true,

	overflowY: 'auto',

	closeBtnInside: true,
	preloader: false,

	midClick: true,
	removalDelay: 300,
	mainClass: 'my-mfp-zoom-in',
	callbacks: {
		open: function(){
			$('.auto-input').val(auto_id);
		}
	}
});
// 

// Подключение формы GoldCarrot
	$('.feedback').goldCarrotForm({
        url: '/mail/feedback.php'
    });
	$('.auto-form').goldCarrotForm({
        url: '/mail/auto-form.php'
    });
	$('.auto-form2').goldCarrotForm({
        url: '/mail/auto-form2.php'
    });
	$('.hotel_form').goldCarrotForm({
        url: '/mail/hotel.php'
    });
	$('.call_request').goldCarrotForm({
        url: '/mail/call_request.php'
    });
//

	$('.select_city').change(function(){
		window.location.href = '/site/change-city/?city_id='+ $(this).val();
	})

});


$('#reviews-form button').on('click', function(e){
	var _this = $(this);
	e.preventDefault();
	$.ajax({
		type: 'post',
		url: _this.prop('action'),
		data: $('#reviews-form').serialize(),
		dataType: 'json',
		success: function(data){
			if (data.status == 'ok') {
				$('#reviews-form').trigger('reset');
				$('#rating').find('.active').removeClass('confirm').css({
					'width': 0
				});
				$('.error-message').html('Спасибо) Ваш отзыв принят)')
			} else {
				$('.error-message').html(data.error_message);
			}
		}
	})

});
let page = 1;
$('#show-reviews').on('click', function(){
	var _this = $(this);
	$.ajax({
		type: 'post',
		url: '/car/show-reviews',
		data: {
			page: page,
			auto_id: _this.data('auto_id')
		},
		dataType: 'json',
		success: function(data){
			if (data.end == 1) {
				$('#show-reviews').hide();
			}
			$('.reviews-wrapper').append(data.reviews);
			page = data.page;
		}
	})
});

// RATING

var stars      = $('#rating'),
	starsWidth = $('#rating').width(),
	widthCalc  = starsWidth/5,
    check      = 0;

$('#rating').hover(function(){
    check = 1;
}, function(){
    check = 0;
});


function rating(){
    var hoverStart   = 0,
        stars        = $('#rating'),
        ratingStars  = stars.find('.active');
        

    $(stars).hover(function(){
        if (!($(this).find('.active').hasClass('confirm'))) {

            hoverStart = 1;

        }
        
    }, function(){
        hoverStart = 0;
        if (!($(this).find('.active').hasClass('confirm')))
        {

            $(this).find('.active').css({
                'width': 0
            })

        }
    });

    $(stars).mousemove(function(e){
        hoverStart = 0;
        //if (!($(this).find('.active').hasClass('confirm')))
        {

            var parentOffset = $(this).offset();
            relXClick = e.pageX - parentOffset.left;

            if (relXClick <= widthCalc) {
                relXClick = widthCalc
            } else if (relXClick <= widthCalc*2) {
                relXClick = widthCalc*2
            } else if (relXClick <= widthCalc*3) {
                relXClick = widthCalc*3
            } else if (relXClick <= widthCalc*4) {
                relXClick = widthCalc*4
            } else if (relXClick <= widthCalc*5) {
                relXClick = widthCalc*5
            }

            ratingStars.addClass('confirm').css({
                'width': relXClick
            }).data('rating', relXClick);
			ratingVal = relXClick/widthCalc;
            $('#ratingVal').val(parseInt(ratingVal.toFixed(1)) );

        }
        
    });

    $(stars).mousemove(function(e) {

        //if (hoverStart == 1)
        {
            
            var parentOffset = $(this).offset();
            relX = e.pageX - parentOffset.left;
            relX = relX-5;

            if (relX <= widthCalc) {
                relX = widthCalc
            } else if (relX <= widthCalc*2) {
                 relX = widthCalc*2
            } else if (relX <= widthCalc*3) {
                 relX = widthCalc*3
            } else if (relX <= widthCalc*4) {
                 relX = widthCalc*4
            } else if (relX <= widthCalc*5) {
                 relX = widthCalc*5
            }

            $(this).find('.active').css({
                'width': relX
            })

        }
    });

}

rating();