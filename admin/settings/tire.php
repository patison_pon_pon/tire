<?php
switch ($action) {
	case 'add':
	$name = new Add($db, 'tire');
	$pole = [
		['title'=> 'Введите название', 'name' => 'name', 'value'=>$_POST['name'], 'required' => '1', 'type'=> 'input'],
		['title'=> 'Продолжить', 'type' => 'submit'],
	];
	$name->poles = $pole;
	$name->drawForm();
	$name->saveDb(1);
		break;
	case 'edit':
	if (!isset($id)) {
		$edit = new Edit($db, 'tire');
	}else{
		$name = new Add($db, 'tire');
		$name->readDb($id);
		$pole = [
			['title'=> 'Введите название', 'name' => 'name', 'value'=>$_POST['name'], 'required' => '1', 'type'=> 'input'],
            ['title'=> 'Введите алиас', 'name' => 'alias', 'value'=>$_POST['alias'], 'type'=> 'input'],
            ['title'=> 'Введите описание', 'name' => 'description','value'=>$_POST['description'], 'type' => 'textarea'],
            [
                'title'=> 'Производитель',
                'required' => true,
                'name' => 'manufacturer_id',
                'value'=>$_POST['manufacturer_id'],
                'type'=> 'select',
                'data' => $name->data_helper->getManufacturers()
            ],
            [
                'title'=> 'Диаметр',
                'required' => true,
                'name' => 'diameter_id',
                'value'=>$_POST['diameter_id'],
                'type'=> 'select',
                'data' => $name->data_helper->getDiameters()
            ],
            [
                'title'=> 'Профиль',
                'required' => true,
                'name' => 'profile_id',
                'value'=>$_POST['profile_id'],
                'type'=> 'select',
                'data' => $name->data_helper->getProfiles()
            ],
            [
                'title'=> 'Ширина',
                'required' => true,
                'name' => 'width_id',
                'value'=>$_POST['width_id'],
                'type'=> 'select',
                'data' => $name->data_helper->getWidths()
            ],
            [
                'title'=> 'Сезон',
                'required' => true,
                'name' => 'season',
                'value'=>$_POST['season'],
                'type'=> 'select',
                'data' => $name->data_helper->getSeasons()
            ],
            [
                'title'=> 'Галерея',
                'required' => true,
                'name' => 'images',
                'value'=>$_POST['images'],
                'type'=> 'images',
            ],
			['title'=> 'ОК', 'type' => 'submit'],
		];
		$name->poles = $pole;
		$pole = [
			['title'=> 'ведите название', 'name' => 'name', 'value'=>$_POST['name'], 'required' => '1', 'type'=> 'input'],
            ['title'=> 'Введите алиас', 'name' => 'alias', 'value'=>$_POST['alias'], 'type'=> 'hidden'],
			['title'=> 'ОК', 'type' => 'submit'],
		];
		$name->poles_langs = $pole;
		$name->drawForm();
		$name->saveDb(1);
	}
		break;
	case 'del':
	$del = new Del($db, 'tire');
		break;
}
?>